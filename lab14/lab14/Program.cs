﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;


namespace lab14
{
    internal class Program
    {
        static List<Film> films;
        static void PrintFilms()
        {
            foreach (Film film in films)
            {
                Console.WriteLine(film.Info().Replace('і', 'i'));
            }
            Console.WriteLine();
        }
        static void Main(string[] args)
        {
            films = new List<Film>();
            FileStream fs = new FileStream("bin.towns", FileMode.Open);
            BinaryReader reader = new BinaryReader(fs);
            try
            {
                Film film;
                Console.WriteLine(" Читаємо данi з файлу...\n"); while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    film = new Film(); for (int i = 1; i <= 8; i++)
                    {
                        switch (i)
                        {
                            case 1:
                                film.Name = reader.ReadString();
                                break;
                            case 2:
                                film.Type = reader.ReadString();
                                break;
                            case 3:
                                film.Auditory = reader.ReadString();
                                break;
                            case 4:
                                film.Time = reader.ReadInt32();
                                break;
                            case 5:
                                film.Dohid = reader.ReadDouble();
                                break;
                            case 6:
                                film.Tickets = reader.ReadDouble();
                                break;
                            case 7:
                                film.Vik = reader.ReadBoolean();
                                break;
                            case 8:
                                film.Ukraine = reader.ReadBoolean(); break;
                        }
                    }
                    films.Add(film);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Сталась помилка: {0}", ex.Message);
            }
            finally
            {
                reader.Close();
            }
            Console.WriteLine(" Несортований перелiк фільмів: {0}", films.Count);
            PrintFilms();
            films.Sort();
            Console.WriteLine(" Сортований перелiк фільмів: {0}", films.Count);
            PrintFilms();
            Console.WriteLine(" Додаємо новий запис: Роккі");
            Film townOdesa = new Film("Роккі", "Спорт", "Всі", 120, 4000000, 237, false, true);
            films.Add(townOdesa);
            films.Sort();
            Console.WriteLine(" Перелiк мiст: {0}", films.Count);
            PrintFilms();
            Console.WriteLine(" Видаляємо останнє значення");
            films.RemoveAt(films.Count - 1);
            Console.WriteLine(" Перелiк мiст: {0}", films.Count);
            PrintFilms();
            Console.ReadKey();
        }
    }
}
