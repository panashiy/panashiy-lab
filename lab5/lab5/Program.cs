﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Лабораторна робота #5 Панашія Тараса. 19 Варіант, АЛЕ ВИКОНАВ 28 варіант
namespace lab5
{
    internal class Program
    {
        class Film
        {
            public string Name;
            public string Type;
            public string Auditory;
            public int Time; public double Dohid;
            public double Tickets;
            public bool Vik;
            public bool Ukraine;
            public double SumBuyTickets()
            {
                return Dohid/ Tickets ;
            }
        }
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.Write("Введiть назву фільму: ");
            string sName = Console.ReadLine();
            Console.Write("Введiть жанр фільму: ");
            string sType = Console.ReadLine();
            Console.Write("Введiть цільову аудиторію фільму: ");
            string sAuditory = Console.ReadLine();
            Console.Write("Введiть тривалість фільму (хв): ");
            string sTime = Console.ReadLine();
            Console.Write("Введiть суму касових зборів (грн): ");
            string sDohid = Console.ReadLine();
            Console.Write("Введiть вартість квитка на фільм (грн):");
            string sTickets = Console.ReadLine();
            Console.Write("Чи є у фільмі вікові обмеження? (y-так, n-нi): ");
            ConsoleKeyInfo keyVik = Console.ReadKey();
            Console.WriteLine();
            Console.Write("Чи є показується цей фільм в Україні? (y-так, n-нi): ");
            ConsoleKeyInfo keyUkraine = Console.ReadKey();
            Console.WriteLine();
            Film OurFilm = new Film();
            OurFilm.Name = sName;
            OurFilm.Type = sType;
            OurFilm.Auditory = sAuditory;
            OurFilm.Time = int.Parse(sTime);
            OurFilm.Dohid = double.Parse(sDohid);
            OurFilm.Tickets = double.Parse(sTickets);
            OurFilm.Vik = keyVik.Key == ConsoleKey.Y ? true : false;
            OurFilm.Ukraine = keyUkraine.Key == ConsoleKey.Y ? true : false;
            double SumBuyTickets = OurFilm.SumBuyTickets();
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Данi про об`ект: ");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Назва фільму: " + OurFilm.Name);
            Console.WriteLine("Жанр: " + OurFilm.Type);
            Console.WriteLine("Цільова аудиторія: " + OurFilm.Auditory);
            Console.WriteLine("Тривалість (хв): " +
             OurFilm.Time.ToString());
            Console.WriteLine("Дохід від касових зборів (грн): " +
             OurFilm.Dohid.ToString("0"));
            Console.WriteLine("Варість квитка (грн): " + OurFilm.Tickets);
            Console.WriteLine(OurFilm.Vik ? "У фільмі є вікові обмеження" :
             "У фільмі немає вікових обмеженнь");
            Console.WriteLine(OurFilm.Ukraine ? "Фільм показується в Україні" :
             "Фільм не показується в Україні");
            Console.WriteLine();
            Console.WriteLine("Кількість проданих білетів: " +
            SumBuyTickets.ToString("0"));
            Console.ReadKey();

        }
    }
}
