﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Лабораторна робота #1 Панашія Тараса. 19 Варіант
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);
            double x = xMin;
            double y;
            double sum=0;
            
            while (x <= xMax)
            {
                double x1 = x;
                double x2 = 3 * x;
                y = (5*Math.Sqrt(Math.Pow(x1,3)+Math.Pow(x2,5) - Math.Cos(x2)))/(Math.Sin(x1));
                Console.WriteLine("x = {0} \t\t y = {1} \t\t ", x, y );
                x += dx;
                sum += Math.Pow(y, 3);
                
            
            }
            if (Math.Abs(x - xMax - dx) > 0.0001)
            {
                double x1 = x;
                double x2 = 3 * x;
                y = (5 * Math.Sqrt(Math.Pow(x1, 3) + Math.Pow(x2, 5) - Math.Cos(x2))) / (Math.Sin(x1));
                Console.WriteLine("x = {0}\t\t y = {1}", xMax, y);
                sum += Math.Pow(y, 3);
            }
            
            Console.WriteLine($"Сума кубів всіх розрахованих проміжник значень f(x)=  {sum} ");
            Console.ReadKey();
        }
    }
}
