﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab13
{
    public class Film
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Auditory { get; set; }
        public int Time { get; set; }
        public double Dohid { get; set; }
        public double Tickets { get; set; }
        public bool Vik { get; set; }
        public bool Ukraine { get; set; }
        public double GetYearIncomePerInhabitant()
        {
            return Dohid / Tickets;
        }
        public Film()
        {
        }
        public Film(string name, string type, string auditory,
        int time, double dohid, double tickets,
        bool vik, bool ukraine)
        {
            Name = name;
            Type = type;
            Auditory = auditory;
            Time = time; Dohid = dohid;
            Tickets = tickets;
            Vik = vik;
            Ukraine = ukraine;
        }
    }


}
