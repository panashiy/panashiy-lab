﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab13
{
    public partial class fFilm : Form
    {
        public Film TheFilm;
        public fFilm(Film t)
        {
            TheFilm = t;
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            TheFilm.Name = tbName.Text.Trim();
            TheFilm.Type = tbType.Text.Trim();
            TheFilm.Auditory = tbAuditory.Text.Trim(); TheFilm.Time = int.Parse(tbTime.Text.Trim()); TheFilm.Dohid = double.Parse(tbDohid.Text.Trim());
            TheFilm.Tickets = double.Parse(tbTickets.Text.Trim());
            TheFilm.Vik = chbVik.Checked;
            TheFilm.Ukraine = chbUkraine.Checked;
            DialogResult = DialogResult.OK;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void fTown_Load(object sender, EventArgs e)
        {
            if (TheFilm != null)
            {
                tbName.Text = TheFilm.Name;
                tbType.Text = TheFilm.Type;
                tbAuditory.Text = TheFilm.Auditory;
                tbTime.Text = TheFilm.Time.ToString();
                tbDohid.Text = TheFilm.Dohid.ToString("0.00");
                tbTickets.Text = TheFilm.Tickets.ToString("0.000");
                chbVik.Checked = TheFilm.Vik; chbUkraine.Checked = TheFilm.Ukraine;
            }
        }
    }
}
