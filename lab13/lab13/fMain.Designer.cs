﻿namespace lab13
{
    partial class fMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            toolStrip1 = new ToolStrip();
            btnAdd = new ToolStripButton();
            btnEdit = new ToolStripButton();
            tsSeparator1 = new ToolStripSeparator();
            btnDel = new ToolStripButton();
            btnClear = new ToolStripButton();
            tsSeparator2 = new ToolStripSeparator();
            btnSaveAsText = new ToolStripButton();
            btnSaveAdBinary = new ToolStripButton();
            btnOpenFromText = new ToolStripButton();
            btnOpenFromBinary = new ToolStripButton();
            toolStripSeparator1 = new ToolStripSeparator();
            btnExit = new ToolStripButton();
            gvFilms = new DataGridView();
            bindSrcFilms = new BindingSource(components);
            saveFileDialog = new SaveFileDialog();
            toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)gvFilms).BeginInit();
            ((System.ComponentModel.ISupportInitialize)bindSrcFilms).BeginInit();
            SuspendLayout();
            // 
            // toolStrip1
            // 
            toolStrip1.Items.AddRange(new ToolStripItem[] { btnAdd, btnEdit, tsSeparator1, btnDel, btnClear, tsSeparator2, btnSaveAsText, btnSaveAdBinary, btnOpenFromText, btnOpenFromBinary, toolStripSeparator1, btnExit });
            toolStrip1.Location = new Point(0, 0);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new Size(800, 25);
            toolStrip1.TabIndex = 0;
            toolStrip1.Text = "toolStrip1";
            // 
            // btnAdd
            // 
            btnAdd.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnAdd.Image = (Image)resources.GetObject("btnAdd.Image");
            btnAdd.ImageTransparentColor = Color.Magenta;
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(23, 22);
            btnAdd.Text = "Додати запис про фільм";
            btnAdd.Click += btnAdd_Click;
            // 
            // btnEdit
            // 
            btnEdit.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnEdit.Image = (Image)resources.GetObject("btnEdit.Image");
            btnEdit.ImageTransparentColor = Color.Magenta;
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(23, 22);
            btnEdit.Text = "Редагувати запис";
            btnEdit.Click += btnEdit_Click;
            // 
            // tsSeparator1
            // 
            tsSeparator1.Name = "tsSeparator1";
            tsSeparator1.Size = new Size(6, 25);
            // 
            // btnDel
            // 
            btnDel.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnDel.Image = (Image)resources.GetObject("btnDel.Image");
            btnDel.ImageTransparentColor = Color.Magenta;
            btnDel.Name = "btnDel";
            btnDel.Size = new Size(23, 22);
            btnDel.Text = "Видалити запис";
            btnDel.Click += btnDel_Click;
            // 
            // btnClear
            // 
            btnClear.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnClear.Image = (Image)resources.GetObject("btnClear.Image");
            btnClear.ImageTransparentColor = Color.Magenta;
            btnClear.Name = "btnClear";
            btnClear.Size = new Size(23, 22);
            btnClear.Text = "Очистити дані";
            btnClear.Click += btnClear_Click;
            // 
            // tsSeparator2
            // 
            tsSeparator2.Name = "tsSeparator2";
            tsSeparator2.Size = new Size(6, 25);
            // 
            // btnSaveAsText
            // 
            btnSaveAsText.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnSaveAsText.Image = (Image)resources.GetObject("btnSaveAsText.Image");
            btnSaveAsText.ImageTransparentColor = Color.Magenta;
            btnSaveAsText.Name = "btnSaveAsText";
            btnSaveAsText.Size = new Size(23, 22);
            btnSaveAsText.Text = "toolStripButton2";
            btnSaveAsText.ToolTipText = "Зберегти в текстовому форматі";
            btnSaveAsText.Click += btnSaveAsText_Click;
            // 
            // btnSaveAdBinary
            // 
            btnSaveAdBinary.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnSaveAdBinary.Image = (Image)resources.GetObject("btnSaveAdBinary.Image");
            btnSaveAdBinary.ImageTransparentColor = Color.Magenta;
            btnSaveAdBinary.Name = "btnSaveAdBinary";
            btnSaveAdBinary.Size = new Size(23, 22);
            btnSaveAdBinary.Text = "toolStripButton3";
            btnSaveAdBinary.ToolTipText = "Зберегти в бінарному форматі";
            btnSaveAdBinary.Click += btnSaveAdBinary_Click;
            // 
            // btnOpenFromText
            // 
            btnOpenFromText.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnOpenFromText.Image = (Image)resources.GetObject("btnOpenFromText.Image");
            btnOpenFromText.ImageTransparentColor = Color.Magenta;
            btnOpenFromText.Name = "btnOpenFromText";
            btnOpenFromText.Size = new Size(23, 22);
            btnOpenFromText.Text = "toolStripButton4";
            btnOpenFromText.ToolTipText = "Читати текстові дані";
            btnOpenFromText.Click += btnOpenFromText_Click;
            // 
            // btnOpenFromBinary
            // 
            btnOpenFromBinary.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnOpenFromBinary.Image = (Image)resources.GetObject("btnOpenFromBinary.Image");
            btnOpenFromBinary.ImageTransparentColor = Color.Magenta;
            btnOpenFromBinary.Name = "btnOpenFromBinary";
            btnOpenFromBinary.Size = new Size(23, 22);
            btnOpenFromBinary.Text = "toolStripButton1";
            btnOpenFromBinary.ToolTipText = "Читати бінарні дані";
            btnOpenFromBinary.Click += btnOpenFromBinary_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(6, 25);
            // 
            // btnExit
            // 
            btnExit.DisplayStyle = ToolStripItemDisplayStyle.Image;
            btnExit.Image = (Image)resources.GetObject("btnExit.Image");
            btnExit.ImageTransparentColor = Color.Magenta;
            btnExit.Name = "btnExit";
            btnExit.RightToLeft = RightToLeft.No;
            btnExit.Size = new Size(23, 22);
            btnExit.Text = "Вийти з програми";
            btnExit.Click += btnExit_Click;
            // 
            // gvFilms
            // 
            gvFilms.AllowUserToAddRows = false;
            gvFilms.AllowUserToDeleteRows = false;
            gvFilms.AutoGenerateColumns = false;
            gvFilms.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gvFilms.DataSource = bindSrcFilms;
            gvFilms.Dock = DockStyle.Fill;
            gvFilms.Location = new Point(0, 25);
            gvFilms.Name = "gvFilms";
            gvFilms.ReadOnly = true;
            gvFilms.Size = new Size(800, 425);
            gvFilms.TabIndex = 1;
            // 
            // fMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(gvFilms);
            Controls.Add(toolStrip1);
            Name = "fMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Лабораторна робота №13";
            Load += fMain_Load;
            Resize += fMain_Resize;
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)gvFilms).EndInit();
            ((System.ComponentModel.ISupportInitialize)bindSrcFilms).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ToolStrip toolStrip1;
        private ToolStripButton btnAdd;
        private ToolStripButton btnEdit;
        private ToolStripSeparator tsSeparator1;
        private ToolStripButton btnDel;
        private ToolStripButton btnClear;
        private ToolStripSeparator tsSeparator2;
        private ToolStripButton btnExit;
        private DataGridView gvFilms;
        private BindingSource bindSrcFilms;
        private ToolStripButton btnSaveAsText;
        private ToolStripButton btnSaveAdBinary;
        private ToolStripButton btnOpenFromText;
        private ToolStripButton btnOpenFromBinary;
        private ToolStripSeparator toolStripSeparator1;
        private SaveFileDialog saveFileDialog;
    }
}
