﻿namespace lab13
{
    partial class fFilm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox1 = new GroupBox();
            tbTickets = new TextBox();
            tbDohid = new TextBox();
            tbTime = new TextBox();
            tbAuditory = new TextBox();
            tbType = new TextBox();
            tbName = new TextBox();
            label6 = new Label();
            label5 = new Label();
            label4 = new Label();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            groupBox2 = new GroupBox();
            chbUkraine = new CheckBox();
            chbVik = new CheckBox();
            btnOk = new Button();
            btnCancel = new Button();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(tbTickets);
            groupBox1.Controls.Add(tbDohid);
            groupBox1.Controls.Add(tbTime);
            groupBox1.Controls.Add(tbAuditory);
            groupBox1.Controls.Add(tbType);
            groupBox1.Controls.Add(tbName);
            groupBox1.Controls.Add(label6);
            groupBox1.Controls.Add(label5);
            groupBox1.Controls.Add(label4);
            groupBox1.Controls.Add(label3);
            groupBox1.Controls.Add(label2);
            groupBox1.Controls.Add(label1);
            groupBox1.Location = new Point(12, 12);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(316, 239);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Загальні дані";
            // 
            // tbTickets
            // 
            tbTickets.Location = new Point(167, 185);
            tbTickets.Name = "tbTickets";
            tbTickets.Size = new Size(127, 23);
            tbTickets.TabIndex = 11;
            // 
            // tbDohid
            // 
            tbDohid.Location = new Point(167, 149);
            tbDohid.Name = "tbDohid";
            tbDohid.Size = new Size(127, 23);
            tbDohid.TabIndex = 10;
            // 
            // tbTime
            // 
            tbTime.Location = new Point(167, 118);
            tbTime.Name = "tbTime";
            tbTime.Size = new Size(127, 23);
            tbTime.TabIndex = 9;
            // 
            // tbAuditory
            // 
            tbAuditory.Location = new Point(167, 89);
            tbAuditory.Name = "tbAuditory";
            tbAuditory.Size = new Size(127, 23);
            tbAuditory.TabIndex = 8;
            // 
            // tbType
            // 
            tbType.Location = new Point(167, 54);
            tbType.Name = "tbType";
            tbType.Size = new Size(127, 23);
            tbType.TabIndex = 7;
            // 
            // tbName
            // 
            tbName.Location = new Point(167, 21);
            tbName.Name = "tbName";
            tbName.Size = new Size(127, 23);
            tbName.TabIndex = 6;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(16, 188);
            label6.Name = "label6";
            label6.Size = new Size(101, 15);
            label6.TabIndex = 5;
            label6.Text = "Ціна квитка (грн)";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(16, 152);
            label5.Name = "label5";
            label5.Size = new Size(108, 15);
            label5.TabIndex = 4;
            label5.Text = "Касові збори (грн)";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(16, 121);
            label4.Name = "label4";
            label4.Size = new Size(132, 15);
            label4.TabIndex = 3;
            label4.Text = "Тривалість фільму (хв)";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(16, 89);
            label3.Name = "label3";
            label3.Size = new Size(62, 15);
            label3.TabIndex = 2;
            label3.Text = "Аудиторія";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(16, 57);
            label2.Name = "label2";
            label2.Size = new Size(38, 15);
            label2.TabIndex = 1;
            label2.Text = "Жанр";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(16, 29);
            label1.Name = "label1";
            label1.Size = new Size(82, 15);
            label1.TabIndex = 0;
            label1.Text = "Назва фільму";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(chbUkraine);
            groupBox2.Controls.Add(chbVik);
            groupBox2.Location = new Point(12, 257);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(316, 100);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            // 
            // chbUkraine
            // 
            chbUkraine.AutoSize = true;
            chbUkraine.Location = new Point(16, 60);
            chbUkraine.Name = "chbUkraine";
            chbUkraine.Size = new Size(145, 19);
            chbUkraine.TabIndex = 13;
            chbUkraine.Text = "Показується в Україні";
            chbUkraine.UseVisualStyleBackColor = true;
            // 
            // chbVik
            // 
            chbVik.AutoSize = true;
            chbVik.Location = new Point(16, 35);
            chbVik.Name = "chbVik";
            chbVik.Size = new Size(134, 19);
            chbVik.TabIndex = 12;
            chbVik.Text = "Є вікові обмеження";
            chbVik.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            btnOk.Location = new Point(346, 24);
            btnOk.Name = "btnOk";
            btnOk.Size = new Size(82, 22);
            btnOk.TabIndex = 2;
            btnOk.Text = "Ок";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += btnOk_Click;
            // 
            // btnCancel
            // 
            btnCancel.Location = new Point(346, 66);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(82, 23);
            btnCancel.TabIndex = 3;
            btnCancel.Text = "Скасувати";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // fTown
            // 
            AcceptButton = btnOk;
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new Size(440, 367);
            Controls.Add(btnCancel);
            Controls.Add(btnOk);
            Controls.Add(groupBox2);
            Controls.Add(groupBox1);
            MaximizeBox = false;
            Name = "fTown";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Дані про місто";
            Load += fTown_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private GroupBox groupBox1;
        private TextBox tbName;
        private Label label6;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private TextBox tbTickets;
        private TextBox tbDohid;
        private TextBox tbTime;
        private TextBox tbAuditory;
        private TextBox tbType;
        private GroupBox groupBox2;
        private CheckBox chbUkraine;
        private CheckBox chbVik;
        private Button btnOk;
        private Button button2;
        private Button btnCancel;
    }
}