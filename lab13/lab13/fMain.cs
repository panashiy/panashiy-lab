using System.Text;
using System.Windows.Forms;

namespace lab13
{

    public partial class fMain : Form
    {
        OpenFileDialog openFileDialog;
        public fMain()
        {
            InitializeComponent();
        }

        private void fMain_Resize(object sender, EventArgs e)
        {
            int buttonsSize = 10 * btnAdd.Width + 4 * tsSeparator1.Width;
            btnExit.Margin = new Padding(Width - buttonsSize, 0, 0, 0);
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvFilms.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "�����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Type";
            column.Name = "����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Auditory";
            column.Name = "��������";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Time";
            column.Name = "���������";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Dohid";
            column.Name = "����� �����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Tickets";
            column.Name = "ֳ�� ������";
            column.Width = 80;
            gvFilms.Columns.Add(column);
            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "Vik";
            column.Name = "³��� ���������";
            column.Width = 60;
            gvFilms.Columns.Add(column);
            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "Ukraine";
            column.Name = "���������� � ������";
            column.Width = 60;
            gvFilms.Columns.Add(column);
            bindSrcFilms.Add(new Film("������ ����", "�����", "��", 140,
             20000002, 400, false, true));
            EventArgs args = new EventArgs();
            OnResize(args);

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            fFilm ft = new fFilm(film);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.Add(film);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Film film = (Film)bindSrcFilms.List[bindSrcFilms.Position];
            fFilm ft = new fFilm(film);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.List[bindSrcFilms.Position] = film;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("�������� �������� �����?",
            "��������� ������", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcFilms.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("�������� �������?\n\n�� ���� ������ ��������",
             "�������� �����", MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcFilms.Clear();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("������� ����������?", "����� � ��������",
            MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "������� ����� (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "�������� ���� � ���������� ������";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Film film in bindSrcFilms.List)
                    {
                        sw.Write(film.Name + "\t" + film.Type + "\t" +
                        film.Auditory + "\t" + film.Time + "\t" + film.Dohid + "\t" + film.Tickets + "\t" +
                        film.Vik + "\t" + film.Ukraine + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("������� �������: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void btnSaveAdBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "����� ����� (*.towns)|*.towns|All files (*.*)|*.*";
            saveFileDialog.Title = "�������� ���� � �������� ������";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new BinaryWriter(saveFileDialog.OpenFile());
                try
                {
                    foreach (Film film in bindSrcFilms.List)
                    {
                        bw.Write(film.Name);
                        bw.Write(film.Type);
                        bw.Write(film.Auditory); bw.Write(film.Time); bw.Write(film.Dohid);
                        bw.Write(film.Tickets);
                        bw.Write(film.Vik);
                        bw.Write(film.Ukraine);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("������� �������: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    bw.Close();
                }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "������� ����� (*.txt)|*.txt|All files (*.*) | *.* ";
            openFileDialog.Title = "��������� ���� � ���������� ������";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.Clear(); sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Film film = new Film(split[0], split[1], split[2],
                        int.Parse(split[3]), double.Parse(split[4]), double.Parse(split[5]), bool.Parse(split[6]), bool.Parse(split[7]));
                        bindSrcFilms.Add(film);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("������� �������: \n{0}", ex.Message,
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sr.Close();
                }
            }
        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "����� ����� (*.towns)|*.towns|All files (*.*) | *.* ";
            openFileDialog.Title = "��������� ���� � �������� ������";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Film film; while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        film = new Film();
                        for (int i = 1; i <= 8; i++)
                        {
                            switch (i)
                            {
                                case 1:
                                    film.Name = br.ReadString();
                                    break;
                                case 2:
                                    film.Type = br.ReadString(); break;
                                case 3:
                                    film.Auditory = br.ReadString(); break;
                                case 4:
                                    film.Time = br.ReadInt32();
                                    break;
                                case 5:
                                    film.Dohid = br.ReadDouble();
                                    break;
                                case 6:
                                    film.Tickets = br.ReadDouble();
                                    break;
                                case 7:
                                    film.Vik = br.ReadBoolean();
                                    break;
                                case 8:
                                    film.Ukraine = br.ReadBoolean();
                                    break;
                            }
                        }
                        bindSrcFilms.Add(film);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("������� �������: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    br.Close();
                }
            }
        }
    }
    }

