namespace lab10
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void fMain_Resize(object sender, EventArgs e)
        {
            int buttonsSize = 5 * btnAdd.Width + 2 * tsSeparator1.Width + 30;
            btnExit.Margin = new Padding(Width - buttonsSize, 0, 0, 0);
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvFilms.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "�����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Type";
            column.Name = "����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Auditory";
            column.Name = "��������";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Time";
            column.Name = "���������";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Dohid";
            column.Name = "����� �����";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Tickets";
            column.Name = "ֳ�� ������";
            column.Width = 80;
            gvFilms.Columns.Add(column);
            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "Vik";
            column.Name = "³��� ���������";
            column.Width = 60;
            gvFilms.Columns.Add(column);
            column = new DataGridViewCheckBoxColumn();
            column.DataPropertyName = "Ukraine";
            column.Name = "���������� � ������";
            column.Width = 60;
            gvFilms.Columns.Add(column);
            bindSrcFilms.Add(new Film("������ ����", "�����", "��", 140,
             20000002, 400, false, true));
            EventArgs args = new EventArgs();
            OnResize(args);

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            fFilm ft = new fFilm(film);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.Add(film);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Film film = (Film)bindSrcFilms.List[bindSrcFilms.Position];
            fFilm ft = new fFilm(film);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilms.List[bindSrcFilms.Position] = film;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("�������� �������� �����?",
            "��������� ������", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcFilms.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("�������� �������?\n\n�� ���� ������ ��������",
             "�������� �����", MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcFilms.Clear();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("������� ����������?", "����� � ��������",
            MessageBoxButtons.OKCancel,
            MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }
    }
}
