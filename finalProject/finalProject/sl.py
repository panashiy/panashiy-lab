﻿import streamlit as st
import requests

API_URL = "http://127.0.0.1:8000"
#Функція для очищення кошика
def clear_cart():
    requests.delete(f"{API_URL}/cart")
    
    #Функція для збереження даних користувача у файл
def save_user_to_file(name: str, car_id: int, filename="user_info.txt"):
    data = f"Ім'я: {name}, ID автомобіля: {car_id}\n"
    with open(filename, 'a', encoding='utf-8') as file:
        file.write(data)

def main():
    st.title("Продаж запчастин")

    # Очищення кошика при запуску програми
    if 'cart_cleared' not in st.session_state:
        clear_cart()
        st.session_state.cart_cleared = True

    # Авторизація користувача
    st.header("Авторизація")
    name = st.text_input("Введіть ім'я")
    cars = requests.get(f"{API_URL}/cars").json()
    car_titles = [car['title'] for car in cars]
    car_title = st.selectbox("Виберіть марку машини", car_titles)

    if st.button("Увійти"):
        car_id = next(car['ID'] for car in cars if car['title'] == car_title)
        user = requests.post(f"{API_URL}/login", params={"name": name, "car_id": car_id}).json()
        st.session_state.user = user
        st.success("Успішно авторизовано")
        save_user_to_file(name, car_id)  

    if 'user' in st.session_state:
        user = st.session_state.user

        # Відображення таблиці з доступними деталями
        st.header("Доступні деталі")
        details = requests.get(f"{API_URL}/details/{user['ID_car']}").json()
        st.table([[detail['title'], detail['cost']] for detail in details])

        # Випадаючий список для вибору деталі
        detail_titles = [detail['title'] for detail in details]
        detail_title = st.selectbox("Виберіть деталь", detail_titles)

        selected_detail = next((detail for detail in details if detail['title'] == detail_title), None)
        if st.button("Додати до кошика"):
            requests.post(f"{API_URL}/add_to_cart", params={"detail_id": selected_detail['ID']})
            st.success("Деталь додана до кошика")

        # Кнопка для очищення кошика
        if st.button("Очистити кошик"):
            clear_cart()
            st.success("Кошик очищено")

        # Показати/приховати кошик
        if 'show_cart' not in st.session_state:
            st.session_state.show_cart = False

        if st.button("Показати/Приховати кошик"):
            st.session_state.show_cart = not st.session_state.show_cart

        if st.session_state.show_cart:
            st.header("Кошик")
            cart = requests.get(f"{API_URL}/cart").json()
            cart_items = []
            for item in cart:
                detail_id = item['ID_detail']
                detail = next((detail for detail in details if detail['ID'] == detail_id), None)
                cart_items.append({"Назва деталі": detail['title'], "Ціна": detail['cost']})
            st.table(cart_items)

if __name__ == "__main__":
    main()
