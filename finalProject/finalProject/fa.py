﻿from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from main import SessionLocal, User, Car, Detail, Cart, Base, engine

app = FastAPI()

Base.metadata.create_all(bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Збереження даних користувача у файл
def save_user_to_file(name: str, car_id: int, filename="user_info.txt"):
    data = f"Ім'я: {name}, ID автомобіля: {car_id}\n"
    with open(filename, 'a', encoding='utf-8') as file:
        file.write(data)

# Endpoint для авторизації
@app.post("/login")
def login(name: str, car_id: int, db: Session = Depends(get_db)):
    user = User(name=name, ID_car=car_id)
    db.add(user)
    db.commit()
    db.refresh(user)
    save_user_to_file(name, car_id)  # Виклик функції для збереження у файл
    return user

@app.get("/cars")
def get_cars(db: Session = Depends(get_db)):
    return db.query(Car).all()

@app.get("/details/{car_id}")
def get_details(car_id: int, db: Session = Depends(get_db)):
    return db.query(Detail).filter(Detail.ID_car == car_id).all()

@app.post("/add_to_cart")
def add_to_cart(detail_id: int, db: Session = Depends(get_db)):
    cart_item = Cart(ID_detail=detail_id)
    db.add(cart_item)
    db.commit()
    db.refresh(cart_item)
    return cart_item

@app.get("/cart")
def get_cart(db: Session = Depends(get_db)):
    cart_items = db.query(Cart).all()
    return [{"ID": item.ID, "ID_detail": item.ID_detail} for item in cart_items]

@app.delete("/cart")
def clear_cart(db: Session = Depends(get_db)):
    db.query(Cart).delete()
    db.commit()
    return {"message": "Cart cleared"}
