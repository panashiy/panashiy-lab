﻿
import os
import tempfile
from fa import save_user_to_file

def test_save_user_to_file():
    
    with tempfile.NamedTemporaryFile(delete=False) as temp_file:
        temp_filename = temp_file.name

    try:
        
        save_user_to_file("Тест", 123, temp_filename)

        
        assert os.path.exists(temp_filename), "Файл не створений"

        
        with open(temp_filename, 'r', encoding='utf-8') as file:
            data = file.read()

        
        assert "Ім'я: Тест, ID автомобіля: 123" in data, "Дані записані неправильно"

    finally:
        
        if os.path.exists(temp_filename):
            os.remove(temp_filename)
