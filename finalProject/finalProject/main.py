﻿
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

engine = create_engine('sqlite:///detailShop.db')
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

class User(Base):
    __tablename__ = "users"
    ID = Column(Integer, primary_key=True, index=True)
    ID_car = Column(Integer, ForeignKey("car.ID"))
    name = Column(String, index=True)

class Car(Base):
    __tablename__ = "car"
    ID = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)

class Detail(Base):
    __tablename__ = "detail"
    ID = Column(Integer, primary_key=True, index=True)
    ID_car = Column(Integer, ForeignKey("car.ID"))
    title = Column(String, index=True)
    cost = Column(Integer, index=True)

class Cart(Base):
    __tablename__ = "cart"
    ID = Column(Integer, primary_key=True, index=True)
    ID_detail = Column(Integer, ForeignKey("detail.ID"))

Base.metadata.create_all(bind=engine)

