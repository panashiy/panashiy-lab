﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{  //Лабораторна робота #3 Панашія Тараса. 19 Варіант
    internal class Program
    {
        static double Function(double x)
        {
            double x1 = 2 * x;
            double x2 = 5 * x;
            return Math.Cos(Math.Sqrt(x2) + 34 * x1) - 4 * Math.Sin(x2);
        }

        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

        StartOfCalculations:
            Console.Write("Введiть початок вiдрiзку iнтегрування a: ");
            string sa = Console.ReadLine();
            double a = double.Parse(sa);
            Console.Write("Введiть кiнець вiдрiзку iнтегрування b: ");
            string sb = Console.ReadLine();
            double b = double.Parse(sb);
            Console.Write("Введiть кiлькiсть дiлянок n: ");
            string sn = Console.ReadLine();
            double n = double.Parse(sn);

            double dx = (b - a) / n;
            double y1, y2;
            double x1, x2;
            double Intgrl = 0;

            for (int i = 0; i < n; i++)
            {
                x1 = a + i * dx;
                x2 = x1 + dx;
                y1 = Function(x1);
                y2 = Function(x2);
                // Обчислення інтегралу методом трапецій
                Intgrl += (y1 + y2) / 2 * dx;
            }

            Console.WriteLine("Iнтеграл функцiї на вiдрiзку [{0}, {1}] становить {2:0.000000}", a, b, Intgrl);
            Console.Write("Повторити розрахунок (y - так) ? ");
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.Y)
            {
                Console.WriteLine();
                goto StartOfCalculations;
            }
        }
    }
}
