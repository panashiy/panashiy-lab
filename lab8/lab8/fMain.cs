namespace lab8
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnAddTown_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            fFilm ft = new fFilm(film);
            if (ft.ShowDialog() == DialogResult.OK)
            {
                tbTownsInfo.Text +=
                string.Format("����� ������: {0},  ���� ������: {1},  ��������: {2}. ���������: {3} ��. ����� �����: {4} ���. ֳ�� ������: {5} ���. [{6} | {7}] | �-���� �������� ������: {8:0} \r\n",
                film.Name, film.Type, film.Auditory,
                film.Time, film.Dohid, film.Tickets,
                film.Vik ? "� ���� ���������" : "���� ������ ��������",
                film.Ukraine ? "Գ��� ���������� � ������" : "Գ��� �� ���������� � ������", film.SumBuyTickets());

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("��������� ������ ����������?","��������� ������", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }
    }
}
