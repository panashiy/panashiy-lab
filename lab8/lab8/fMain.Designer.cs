﻿namespace lab8
{
    partial class fMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbTownsInfo = new TextBox();
            btnAddFilm = new Button();
            btnClose = new Button();
            SuspendLayout();
            // 
            // tbTownsInfo
            // 
            tbTownsInfo.Location = new Point(12, 12);
            tbTownsInfo.Multiline = true;
            tbTownsInfo.Name = "tbTownsInfo";
            tbTownsInfo.ReadOnly = true;
            tbTownsInfo.Size = new Size(838, 272);
            tbTownsInfo.TabIndex = 0;
            // 
            // btnAddFilm
            // 
            btnAddFilm.Location = new Point(859, 12);
            btnAddFilm.Name = "btnAddFilm";
            btnAddFilm.Size = new Size(98, 34);
            btnAddFilm.TabIndex = 1;
            btnAddFilm.Text = "Додати фільм";
            btnAddFilm.UseVisualStyleBackColor = true;
            btnAddFilm.Click += btnAddTown_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(859, 252);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(98, 32);
            btnClose.TabIndex = 2;
            btnClose.Text = "Закрити";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // fMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(969, 296);
            Controls.Add(btnClose);
            Controls.Add(btnAddFilm);
            Controls.Add(tbTownsInfo);
            MinimizeBox = false;
            Name = "fMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Лабораторна робота №8";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbTownsInfo;
        private Button btnAddFilm;
        private Button btnClose;
    }
}
