namespace lab7
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbX1.Text) ||
            (String.IsNullOrEmpty(tbX2.Text)))
            {
                tbY.Text = "�� ������� �����!";
                return;
            }
            double x1 = double.Parse(tbX1.Text);
            double x2 = double.Parse(tbX2.Text);
            double y = (Math.Pow(Math.Cos(x1),3)+45+x2)/(Math.Pow(x1,13)+Math.Cos(x2));
            tbY.Text = y.ToString("E2");
            
            if (x1 > x2)
            {
                tbAvg.Text = x1.ToString("E2");
            }
            else
            {
                tbAvg.Text = x2.ToString("E2");
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbX1.Text = string.Empty;
            tbX2.Text = string.Empty;
            tbY.Text = string.Empty;
            tbAvg.Text = string.Empty;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
