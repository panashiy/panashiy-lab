﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{   // Л.Р #4 Панашія Тараса. 19 Варіант.
    internal class Program
    {
        const double StartX = 10.3;
        const double dX = 0.7;
        static double Function(double x)
        {
            double x1 =2.76 * x;
            double x2 =0.5 * x;
            return Math.Sin(x1-x2+Math.Sqrt(x1))-1.3*Math.Pow(x1,3 );
        }
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            double[] arr = new double[10];
            double x = StartX;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = Function(x);
                x += dX;
            }
            Array.Sort(arr);
            Array.Reverse(arr);
            Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                Console.WriteLine("arr[{0:00}] = {1:0.0000}", i, arr[i]);
            }
            double aMin = arr[arr.GetUpperBound(0)];
            double aMax = arr[arr.GetLowerBound(0)];
            double aAvg = 0;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                aAvg += arr[i];
            }
            aAvg = aAvg / arr.GetLength(0);

            double range = 0.1 * aAvg; // 10% від середнього значення
            double lowerBound = aAvg - Math.Abs(range);
            double upperBound = aAvg + Math.Abs(range);

            double sumInRange = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] >= lowerBound && arr[i] <= upperBound)
                {
                    sumInRange += arr[i];
                }
            }

            Console.WriteLine("Мiнiмальне значення масиву: {0:0.0000}", aMin);
            Console.WriteLine("Максимальне значення масиву: {0:0.0000}", aMax);
            Console.WriteLine("Середнє значення масиву: {0:0.0000}", aAvg);
            Console.WriteLine("Сума елементів масиву в діапазоні від {0:0.0000} до {1:0.0000}: {2:0.0000}", lowerBound, upperBound, sumInRange);
            Console.ReadKey(true);


        }
    }
}
