﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6lib
{
    public class Film
    {
        public string Name;
        public string Type;
        public string Auditory;
        public int Time; 
        public double Dohid;
        public double Tickets;
        public bool Vik;
        public bool Ukraine;


        public double SumBuyTickets
        {
            get
            {
                return GetSumBuyTickets();
            }
        }
        public double GetSumBuyTickets()
        {
            return Dohid / Tickets;
        }
    }
}
