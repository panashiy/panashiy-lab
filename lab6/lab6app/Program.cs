﻿using lab6lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6app
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;


            Film[] arrFilms;
            Console.Write("Введiть кiлькiсть фільмів: ");
            int cntFilms = int.Parse(Console.ReadLine());
            arrFilms = new Film[cntFilms];
            for (int i = 0; i < cntFilms; i++)
            {
                Console.Write("Введiть назву фільму: ");
                string sName = Console.ReadLine();
                Console.Write("Введiть жанр фільму: ");
                string sType = Console.ReadLine();
                Console.Write("Введiть цільову аудиторію фільму: ");
                string sAuditory = Console.ReadLine();
                Console.Write("Введiть тривалість фільму (хв): ");
                string sTime = Console.ReadLine();
                Console.Write("Введiть суму касових зборів (грн): ");
                string sDohid = Console.ReadLine();
                Console.Write("Введiть вартість квитка на фільм (грн):");
                string sTickets = Console.ReadLine();
                Console.Write("Чи є у фільмі вікові обмеження? (y-так, n-нi): ");
                ConsoleKeyInfo keyVik = Console.ReadKey();
                Console.WriteLine();
                Console.Write("Чи є показується цей фільм в Україні? (y-так, n-нi): ");
                ConsoleKeyInfo keyUkraine = Console.ReadKey();
                Console.WriteLine();
                Console.WriteLine();

                Film OurFilm = new Film();
                OurFilm.Name = sName;
                OurFilm.Type = sType;
                OurFilm.Auditory = sAuditory;
                OurFilm.Time = int.Parse(sTime);
                OurFilm.Dohid = double.Parse(sDohid);
                OurFilm.Tickets = double.Parse(sTickets);
                OurFilm.Vik = keyVik.Key == ConsoleKey.Y ? true : false;
                OurFilm.Ukraine = keyUkraine.Key == ConsoleKey.Y ? true : false;

                arrFilms[i] = OurFilm ;
            }
            foreach (Film t in arrFilms)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Данi про фільм {0}", t.Name); Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Жанр: " + t.Type); Console.WriteLine("Цільова аудиторія: " + t.Auditory); Console.WriteLine("Тривалість (хв): " +
                t.Time.ToString());
                Console.WriteLine("Касові збори: " + t.Dohid.ToString("0.00"));
                Console.WriteLine("Вартість квитка: " + t.Tickets.ToString("0.000")); Console.WriteLine(t.Vik ? "У фільмі є вікові обмеження" :
                "У фільмі немає вікових обмеженнь");
                Console.WriteLine(t.Ukraine ? "Фільм показується в Україні" : "Фільм не показується в Україні"); Console.WriteLine();
                Console.WriteLine("Кількість проданих білетів: " + t.SumBuyTickets.ToString("0.00"));
            }
            Console.ReadKey();




        }
    }
}
