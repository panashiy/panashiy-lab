﻿using System.Drawing;

namespace lab12
{
    abstract class CFigure
    {
        // Поля
        protected Graphics graphics;

        public int X { get; set; }// Координата X центра фігури
        public int Y { get; set; }// Координата Y центра фігури
        // Абстрактний метод: малює фігуру на поверхні малювання GDI+
        abstract protected void Draw(Pen pen);
        // Показує фігуру (малює на поверхні малювання GDI+ кольором
        // переднього плану)
        public void Show()
        {
            Draw(Pens.Red);
        }
        // Приховує фігуру (малює на поверхні малювання GDI+ кольором фону)
        public void Hide()
        {
            Draw(Pens.White);
        }
        // Переміщує фігуру
        public void Move(int dX, int dY)
        {
            Hide();
            X += dX;
            Y += dY;
            Show();
        }
        // Розширює фігуру
        abstract public void Expand(int dR);
        // Стискає фігуру
        abstract public void Collapse(int dR);
    }
}
