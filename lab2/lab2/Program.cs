﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Лабораторна робота #2 Панашія Тараса. 19 Варіант
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            Console.Write("Введiть початкове значення X1min: ");
            string sx1Min = Console.ReadLine();
            double x1Min = Double.Parse(sx1Min);
            Console.Write("Введiть кiнцеве значення X1max: ");
            string sx1Max = Console.ReadLine();
            double x1Max = double.Parse(sx1Max);
            Console.Write("Введiть прирiст dX1: ");
            string sdx1 = Console.ReadLine();
            double dx1 = double.Parse(sdx1);

            Console.Write("Введiть початкове значення X2min: ");
            string sx2Min = Console.ReadLine();
            double x2Min = Double.Parse(sx2Min);
            Console.Write("Введiть кiнцеве значення X2max: ");
            string sx2Max = Console.ReadLine();
            double x2Max = double.Parse(sx2Max);
            Console.Write("Введiть прирiст dX2: ");
            string sdx2 = Console.ReadLine();
            double dx2 = double.Parse(sdx2);
            double y;
            double x1 = x1Min;
            double x2;
            double sum = 0;
            double kube= 0;
            while (x1 <= x1Max)
            {
                x2 = x2Min;
                while (x2 <= x2Max)
                {
                    y = Math.Exp(x1-Math.Pow(x2,2))+31.55*x2*Math.Pow(x1,2);
                    Console.WriteLine("x1 = {0:E2}\tx2 = {1:E2}\t\ty = {2:E2}", x1, x2, y);
                    x2 += dx2;
                     kube = Math.Pow(y, 3);
                    if ( kube>=0)
                    {
                        sum +=Math.Pow(y,3);

                    }
                }
                x1 += dx1;
            }
            Console.WriteLine($"Сума додатних кубів всіх розрахованих проміжних значень f(x1, x2)={sum:E2} ");
            Console.ReadKey();
        }
    }
}
