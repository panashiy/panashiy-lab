﻿using System;
using System.Drawing;

namespace lab11
{
    class CCircle
    {
        const int DefaultRadius = 50;

        private Graphics graphics;
        private int _radius;

        public int X { get; set; }
        public int Y { get; set; }
        public int Radius
        {
            get => _radius;
            set => _radius = value >= 200 ? 200 : (value <= 5 ? 5 : value);
        }

        public CCircle(Graphics graphics, int X, int Y)
        {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this.Radius = DefaultRadius;
        }

        public CCircle(Graphics graphics, int X, int Y, int Radius)
        {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this.Radius = Radius;
        }

        public void Show()
        {
            DrawEmblem(Pens.Red);
        }

        public void Hide()
        {
            DrawEmblem(Pens.White);
        }

        public void Expand()
        {
            Hide();
            Radius++;
            Show();
        }

        public void Expand(int dR)
        {
            Hide();
            Radius += dR;
            Show();
        }

        public void Collapse()
        {
            Hide();
            Radius--;
            Show();
        }

        public void Collapse(int dR)
        {
            Hide();
            Radius -= dR;
            Show();
        }

        public void Move(int dX, int dY)
        {
            Hide();
            X += dX;
            Y += dY;
            Show();
        }

        private void DrawEmblem(Pen pen)
        {
            int triangleHeight = (int)(Math.Sqrt(3) * Radius / 2); // Height of equilateral triangle
            int halfSquare = Radius;

            // Triangle
            Point triangleTop = new Point(X - Radius, Y - triangleHeight);
            Point triangleLeft = new Point(X - (int)(Radius * 3), Y);
            Point triangleRight = new Point(X - Radius, Y + triangleHeight);
            Point[] trianglePoints = { triangleTop, triangleLeft, triangleRight };
            graphics.DrawPolygon(pen, trianglePoints);

            // Circle
            Rectangle circleRectangle = new Rectangle(X - Radius, Y - Radius, Radius * 2, Radius * 2);
            graphics.DrawEllipse(pen, circleRectangle);

            // Square
            Rectangle squareRectangle = new Rectangle(X + halfSquare, Y - halfSquare, halfSquare * 2, halfSquare * 2);
            graphics.DrawRectangle(pen, squareRectangle);
        }
    }
}