﻿namespace lab11
{
    partial class fMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnMain = new Panel();
            pnTools = new Panel();
            btnDown = new Button();
            btnUpFar = new Button();
            btnDownFar = new Button();
            btnRightFar = new Button();
            btnRight = new Button();
            btnLeftFar = new Button();
            btnLeft = new Button();
            btnCollapse = new Button();
            btnExpand = new Button();
            btnUp = new Button();
            btnShow = new Button();
            btnHide = new Button();
            btnCreateNew = new Button();
            cbCircles = new ComboBox();
            label1 = new Label();
            pnTools.SuspendLayout();
            SuspendLayout();
            // 
            // pnMain
            // 
            pnMain.BackColor = Color.White;
            pnMain.BorderStyle = BorderStyle.FixedSingle;
            pnMain.Dock = DockStyle.Fill;
            pnMain.Location = new Point(0, 0);
            pnMain.Name = "pnMain";
            pnMain.Size = new Size(800, 450);
            pnMain.TabIndex = 0;
            // 
            // pnTools
            // 
            pnTools.Controls.Add(btnDown);
            pnTools.Controls.Add(btnUpFar);
            pnTools.Controls.Add(btnDownFar);
            pnTools.Controls.Add(btnRightFar);
            pnTools.Controls.Add(btnRight);
            pnTools.Controls.Add(btnLeftFar);
            pnTools.Controls.Add(btnLeft);
            pnTools.Controls.Add(btnCollapse);
            pnTools.Controls.Add(btnExpand);
            pnTools.Controls.Add(btnUp);
            pnTools.Controls.Add(btnShow);
            pnTools.Controls.Add(btnHide);
            pnTools.Controls.Add(btnCreateNew);
            pnTools.Controls.Add(cbCircles);
            pnTools.Controls.Add(label1);
            pnTools.Dock = DockStyle.Right;
            pnTools.Location = new Point(477, 0);
            pnTools.Name = "pnTools";
            pnTools.Size = new Size(323, 450);
            pnTools.TabIndex = 1;
            // 
            // btnDown
            // 
            btnDown.Location = new Point(120, 364);
            btnDown.Name = "btnDown";
            btnDown.Size = new Size(76, 30);
            btnDown.TabIndex = 16;
            btnDown.Text = "↓";
            btnDown.UseVisualStyleBackColor = true;
            btnDown.Click += btnDown_Click;
            // 
            // btnUpFar
            // 
            btnUpFar.Location = new Point(120, 221);
            btnUpFar.Name = "btnUpFar";
            btnUpFar.Size = new Size(76, 25);
            btnUpFar.TabIndex = 15;
            btnUpFar.Text = "↑↑";
            btnUpFar.UseVisualStyleBackColor = true;
            btnUpFar.Click += btnUpFar_Click;
            // 
            // btnDownFar
            // 
            btnDownFar.Location = new Point(120, 400);
            btnDownFar.Name = "btnDownFar";
            btnDownFar.Size = new Size(76, 25);
            btnDownFar.TabIndex = 13;
            btnDownFar.Text = "↓↓";
            btnDownFar.UseVisualStyleBackColor = true;
            btnDownFar.Click += btnDownFar_Click;
            // 
            // btnRightFar
            // 
            btnRightFar.Location = new Point(243, 286);
            btnRightFar.Name = "btnRightFar";
            btnRightFar.Size = new Size(24, 72);
            btnRightFar.TabIndex = 12;
            btnRightFar.Text = "→→";
            btnRightFar.UseVisualStyleBackColor = true;
            btnRightFar.Click += btnRightFar_Click;
            // 
            // btnRight
            // 
            btnRight.Location = new Point(202, 286);
            btnRight.Name = "btnRight";
            btnRight.Size = new Size(35, 72);
            btnRight.TabIndex = 11;
            btnRight.Text = "→";
            btnRight.UseVisualStyleBackColor = true;
            btnRight.Click += btnRight_Click;
            // 
            // btnLeftFar
            // 
            btnLeftFar.Location = new Point(49, 286);
            btnLeftFar.Name = "btnLeftFar";
            btnLeftFar.Size = new Size(24, 72);
            btnLeftFar.TabIndex = 10;
            btnLeftFar.Text = "←←";
            btnLeftFar.UseVisualStyleBackColor = true;
            btnLeftFar.Click += btnLeftFar_Click;
            // 
            // btnLeft
            // 
            btnLeft.Location = new Point(79, 286);
            btnLeft.Name = "btnLeft";
            btnLeft.Size = new Size(35, 72);
            btnLeft.TabIndex = 9;
            btnLeft.Text = "←";
            btnLeft.UseVisualStyleBackColor = true;
            btnLeft.Click += btnLeft_Click;
            // 
            // btnCollapse
            // 
            btnCollapse.Location = new Point(120, 325);
            btnCollapse.Name = "btnCollapse";
            btnCollapse.Size = new Size(76, 33);
            btnCollapse.TabIndex = 8;
            btnCollapse.Text = "-";
            btnCollapse.UseVisualStyleBackColor = true;
            btnCollapse.Click += btnCollapse_Click;
            // 
            // btnExpand
            // 
            btnExpand.Location = new Point(120, 286);
            btnExpand.Name = "btnExpand";
            btnExpand.Size = new Size(76, 33);
            btnExpand.TabIndex = 7;
            btnExpand.Text = "+";
            btnExpand.UseVisualStyleBackColor = true;
            btnExpand.Click += btnExpand_Click;
            // 
            // btnUp
            // 
            btnUp.Location = new Point(120, 248);
            btnUp.Name = "btnUp";
            btnUp.Size = new Size(76, 30);
            btnUp.TabIndex = 6;
            btnUp.Text = "↑";
            btnUp.UseVisualStyleBackColor = true;
            btnUp.Click += btnUp_Click;
            // 
            // btnShow
            // 
            btnShow.Location = new Point(38, 159);
            btnShow.Name = "btnShow";
            btnShow.Size = new Size(242, 25);
            btnShow.TabIndex = 4;
            btnShow.Text = "Показати об'єкт";
            btnShow.UseVisualStyleBackColor = true;
            btnShow.Click += btnShow_Click;
            // 
            // btnHide
            // 
            btnHide.Location = new Point(38, 128);
            btnHide.Name = "btnHide";
            btnHide.Size = new Size(242, 25);
            btnHide.TabIndex = 3;
            btnHide.Text = "Приховати об'єкт";
            btnHide.UseVisualStyleBackColor = true;
            btnHide.Click += btnHide_Click;
            // 
            // btnCreateNew
            // 
            btnCreateNew.Location = new Point(38, 97);
            btnCreateNew.Name = "btnCreateNew";
            btnCreateNew.Size = new Size(242, 25);
            btnCreateNew.TabIndex = 2;
            btnCreateNew.Text = "Створити новий об'єкт";
            btnCreateNew.UseVisualStyleBackColor = true;
            btnCreateNew.Click += btnCreateNew_Click;
            // 
            // cbCircles
            // 
            cbCircles.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCircles.FormattingEnabled = true;
            cbCircles.Location = new Point(38, 53);
            cbCircles.Name = "cbCircles";
            cbCircles.Size = new Size(242, 23);
            cbCircles.TabIndex = 1;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(38, 35);
            label1.Name = "label1";
            label1.Size = new Size(97, 15);
            label1.TabIndex = 0;
            label1.Text = "Перелік об'єктів";
            // 
            // fMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(pnTools);
            Controls.Add(pnMain);
            Name = "fMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Лабораторна робота №11";
            Load += fMain_Load;
            pnTools.ResumeLayout(false);
            pnTools.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel pnMain;
        private Panel pnTools;
        private Button btnShow;
        private Button btnHide;
        private Button btnCreateNew;
        private ComboBox cbCircles;
        private Label label1;
        private Button btnLeft;
        private Button btnCollapse;
        private Button btnExpand;
        private Button btnUp;
        private Button btnDownFar;
        private Button btnRightFar;
        private Button btnRight;
        private Button btnLeftFar;
        private Button btnUpFar;
        private Button btnDown;
    }
}
