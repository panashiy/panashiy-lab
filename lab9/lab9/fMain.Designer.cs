﻿namespace lab9
{
    partial class fMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            label4 = new Label();
            gtrf = new Label();
            dfd = new Label();
            tbx1min = new TextBox();
            tbx2min = new TextBox();
            tbx1max = new TextBox();
            tbx2max = new TextBox();
            tbdx1 = new TextBox();
            tbdx2 = new TextBox();
            gv = new DataGridView();
            btnCalc = new Button();
            btnClear = new Button();
            btnExit = new Button();
            label5 = new Label();
            tbSum = new TextBox();
            ((System.ComponentModel.ISupportInitialize)gv).BeginInit();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 20);
            label1.Name = "label1";
            label1.Size = new Size(40, 15);
            label1.TabIndex = 0;
            label1.Text = "x1min";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 50);
            label2.Name = "label2";
            label2.Size = new Size(40, 15);
            label2.TabIndex = 1;
            label2.Text = "x2min";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(151, 20);
            label3.Name = "label3";
            label3.Size = new Size(42, 15);
            label3.TabIndex = 2;
            label3.Text = "x1max";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(151, 50);
            label4.Name = "label4";
            label4.Size = new Size(42, 15);
            label4.TabIndex = 3;
            label4.Text = "x2max";
            // 
            // gtrf
            // 
            gtrf.AutoSize = true;
            gtrf.Location = new Point(285, 20);
            gtrf.Name = "gtrf";
            gtrf.Size = new Size(26, 15);
            gtrf.TabIndex = 4;
            gtrf.Text = "dx1";
            // 
            // dfd
            // 
            dfd.AutoSize = true;
            dfd.Location = new Point(285, 50);
            dfd.Name = "dfd";
            dfd.Size = new Size(26, 15);
            dfd.TabIndex = 5;
            dfd.Text = "dx2";
            // 
            // tbx1min
            // 
            tbx1min.Location = new Point(56, 17);
            tbx1min.Name = "tbx1min";
            tbx1min.Size = new Size(75, 23);
            tbx1min.TabIndex = 6;
            // 
            // tbx2min
            // 
            tbx2min.Location = new Point(56, 47);
            tbx2min.Name = "tbx2min";
            tbx2min.Size = new Size(75, 23);
            tbx2min.TabIndex = 7;
            // 
            // tbx1max
            // 
            tbx1max.Location = new Point(195, 17);
            tbx1max.Name = "tbx1max";
            tbx1max.Size = new Size(71, 23);
            tbx1max.TabIndex = 8;
            // 
            // tbx2max
            // 
            tbx2max.Location = new Point(195, 47);
            tbx2max.Name = "tbx2max";
            tbx2max.Size = new Size(71, 23);
            tbx2max.TabIndex = 9;
            // 
            // tbdx1
            // 
            tbdx1.Location = new Point(329, 17);
            tbdx1.Multiline = true;
            tbdx1.Name = "tbdx1";
            tbdx1.Size = new Size(77, 23);
            tbdx1.TabIndex = 10;
            // 
            // tbdx2
            // 
            tbdx2.Location = new Point(329, 47);
            tbdx2.Name = "tbdx2";
            tbdx2.Size = new Size(77, 23);
            tbdx2.TabIndex = 11;
            // 
            // gv
            // 
            gv.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            gv.Location = new Point(12, 93);
            gv.Name = "gv";
            gv.Size = new Size(553, 217);
            gv.TabIndex = 12;
            // 
            // btnCalc
            // 
            btnCalc.Location = new Point(588, 93);
            btnCalc.Name = "btnCalc";
            btnCalc.Size = new Size(93, 25);
            btnCalc.TabIndex = 13;
            btnCalc.Text = "Розрахувати";
            btnCalc.UseVisualStyleBackColor = true;
            btnCalc.Click += btnCalc_Click;
            // 
            // btnClear
            // 
            btnClear.Location = new Point(588, 135);
            btnClear.Name = "btnClear";
            btnClear.Size = new Size(91, 23);
            btnClear.TabIndex = 14;
            btnClear.Text = "Очистити";
            btnClear.UseVisualStyleBackColor = true;
            btnClear.Click += btnClear_Click;
            // 
            // btnExit
            // 
            btnExit.Location = new Point(588, 287);
            btnExit.Name = "btnExit";
            btnExit.Size = new Size(91, 23);
            btnExit.TabIndex = 15;
            btnExit.Text = "Вийти";
            btnExit.UseVisualStyleBackColor = true;
            btnExit.Click += btnExit_Click;
            // 
            // label5
            // 
            label5.Location = new Point(435, 17);
            label5.Name = "label5";
            label5.Size = new Size(117, 70);
            label5.TabIndex = 16;
            label5.Text = "Сума квадратів проміжних від'ємних значень y";
            label5.Click += label5_Click;
            // 
            // tbSum
            // 
            tbSum.Location = new Point(558, 29);
            tbSum.Multiline = true;
            tbSum.Name = "tbSum";
            tbSum.ReadOnly = true;
            tbSum.Size = new Size(100, 23);
            tbSum.TabIndex = 17;
            tbSum.TextChanged += textBox1_TextChanged;
            // 
            // fMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(706, 322);
            Controls.Add(tbSum);
            Controls.Add(label5);
            Controls.Add(btnExit);
            Controls.Add(btnClear);
            Controls.Add(btnCalc);
            Controls.Add(gv);
            Controls.Add(tbdx2);
            Controls.Add(tbdx1);
            Controls.Add(tbx2max);
            Controls.Add(tbx1max);
            Controls.Add(tbx2min);
            Controls.Add(tbx1min);
            Controls.Add(dfd);
            Controls.Add(gtrf);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(label1);
            Name = "fMain";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Лабораторна робота №9";
            ((System.ComponentModel.ISupportInitialize)gv).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label gtrf;
        private Label dfd;
        private TextBox tbx1min;
        private TextBox tbx2min;
        private TextBox tbx1max;
        private TextBox tbx2max;
        private TextBox tbdx1;
        private TextBox tbdx2;
        private DataGridView gv;
        private Button btnCalc;
        private Button btnClear;
        private Button btnExit;
        private Label label5;
        private TextBox tbSum;
    }
}
